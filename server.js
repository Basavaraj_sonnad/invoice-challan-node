// @ts-check

// Logging Configurations
const fs = require('fs');
const logDir = 'logs';
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}
const logger = require('./config/logger');

const express = require('express');

import {getChallanReqHandler} from './handlers/challanHandler';
import {getInvoiceReqHandler} from './handlers/invoiceHandler';

const app = express();
const bodyParser = require('body-parser');
const env = process.env.NODE_ENV || 'development';

// Set port
app.set('port', process.env.PORT || 3535);

app.set('view engine', 'pug')
app.set('views', './templates');

/**
 * All MiddleWare goes here
 */

app.use(express.static('static'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/**
 * All Endpoints go here
 */

app.get('/node/challan', getChallanReqHandler);
app.get('/node/invoice', getInvoiceReqHandler);

app.use((err, req, res, next) => {
  logger.log('error', `${err.stack}`);

  res.status(err.statusCode).render('error', err);
})

app.listen(app.get('port'), function () {
  logger.log('info', `${env} - Node app listening on port ${app.get('port')}!`);
})
