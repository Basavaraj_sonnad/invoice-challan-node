exports.config = function() {
    let config = {
        'development': {
            API_BASE_URL: "http://192.168.2.21:9898/invoiceprintservice/api",
        },
        'staging': {
            API_BASE_URL: "http://192.168.2.21:9898/invoiceprintservice/api",
        },
        'production': {
            API_BASE_URL: "http://invoice.agrostar.in/invoiceprintservice/api"
        }
    }

    return function(env) {
        env = env || 'development';
        return config[env];
    }
}
