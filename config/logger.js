let winston = require('winston');

let logger = new winston.Logger({
    transports: [
        new winston.transports.File({
            name: 'error-file',
            filename: './logs/node-std-err.log',
            level: 'error',
            json: false
        }),
        new winston.transports.File({
            name: 'info-file',
            filename: './logs/node-std-info.log',
            maxsize: 5242880, //5MB
            maxFiles: 5,
            level: 'debug'
        })
    ],
    exceptionHandlers: [
      new winston.transports.File({ filename: 'logs/node-std-exceptions.log' })
    ],
    exitOnError: false
});

let env = process.env.NODE_ENV || 'development';
if(env === 'production')
    logger.add(
        winston.transports.Console, {
            level: 'info',
            handleExceptions: true,
            json: false,
            colorize: true
        }
    )
else
    logger.add(
        winston.transports.Console, {
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true
        }
    )

module.exports = logger;