# README #

NodeJS Server to create Invoice and Challan.

### What is this repository for? ###

To print invoice and Delivery Challan for packages.

### How do I get set up? ###

* Summary of set up
We need graphicsmagick for barcode generation. So please install - https://www.howtoinstall.co/en/ubuntu/trusty/graphicsmagick
MAC - brew install graphicsmagick
LINUX - sudo apt-get install graphicsmagick

* Configuration
Get Latest Node setup -
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
sudo apt-get install -y nodejs

* Dependencies
npm install -- Install all node dependencies and dev dependencies

* Deployment instructions
For Dev Purpose - npm run dev
For Prod - npm run build followed by npm run serve