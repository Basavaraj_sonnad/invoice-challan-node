const Barcode = require('barcode');
const axios = require('axios');
const _ = require('underscore');
const config = require('../config/env').config()(process.env.NODE_ENV);
const logger = require('../config/logger');

let baseAPIUrl = config.API_BASE_URL + "/dcchallan/";

function _validRequest({facilityId, packageId}) {
    if(!facilityId || !packageId)
        return false;
    return true;
}

function _getDataFromServer({facilityId, packageId}) {
    let params = {packageId, facilityId};

    logger.log('debug', `Making API call with params = ${params}`);
    return axios.get(baseAPIUrl, {
        params: params
    });
}

exports.getChallanReqHandler = function (req, res, next) {
    let facilityId = req.query.facilityId;
    let packageId = req.query.packageId;
    // Handle Client Error
    if(!_validRequest({facilityId, packageId})) {
        let error = new Error();
        error.messageHeader = `Unable to print Challan for Package ${packageId}`;
        error.message = `PackageId or Facility is missing`;
        error.statusCode = 400;
        return next(error);
    }

    _getDataFromServer({facilityId, packageId}).then( response => {  
        let challanData = response.data.responseData;
        let barcodeText = challanData.barcodeText;
        // log request params
        logger.log('debug', `Response from API`, challanData);
        logger.log('debug', `Now generating barcode for value=${challanData.packageCode}`);
        let code128 = Barcode('code128', {
            data: barcodeText,
            width: 230,
            height: 50
        });
      
        code128.getBase64(function (err, barcodeImgSrc) {
            if (err) {
            let error = new Error();
            error.messageHeader = `Unable to print Challan for Package ${packageId}`;
            error.message = `Unable to generate barcode of ${challanData.barcodeText}`;
            error.statusCode = 500;
            return next(error);
            }
            
            challanData.barcode = barcodeImgSrc;

            challanData.isShippedByIndiaPost = function() {
            return ['SELF', 'FRANCHISEE'].indexOf(challanData.shippingProvider.toUpperCase()) === -1;
            }
            challanData.getAddressString = function(address) {
            let keys = ['taluka', 'district', 'state', 'pincode'];
            return _.values(_.pick(address, ...keys)).join(', ');
            }
            res.render('challan', challanData);
        });
    }).catch(function(err) {
        let error = new Error();
        error.messageHeader = `Unable to print Challan for Package ${packageId}`;
        if(err.code === "ECONNREFUSED") {
            error.message = "API server is not responding!!"
        } else {
            error.message = err.response.data.message;
        }
        error.statusCode = 500;
        return next(error);
    });
  };
  
