const Barcode = require('barcode');
const axios = require('axios');
const config = require('../config/env').config()(process.env.NODE_ENV);
const logger = require('../config/logger');

let baseAPIUrl = config.API_BASE_URL + "/invoice/";

function _validateRequest({facilityId, orderId, packageId}) {
    if(!facilityId || !packageId || !orderId)
        return false;
    return true;
}

function _getDataFromServer({facilityId, packageId, orderId}) {
    let params = {packageId, facilityId, orderId};

    logger.log('debug', `Making API call with params = ${JSON.stringify(params)}`);
    return axios.get(baseAPIUrl, {
        params: params
    });
}

exports.getInvoiceReqHandler = function (req, res, next) {
    let facilityId = req.query.facilityId;
    let packageId = req.query.packageId;
    let orderId = req.query.orderId;
    let isValidRequest = _validateRequest({facilityId, packageId, orderId});
    if(! isValidRequest) {
        let error = new Error();
        error.messageHeader = `Unable to print Invoice for Package ${packageId}`;
        error.message = `PackageId or Facility or OrderId is missing`;
        error.statusCode = 400;
        return next(error);
    }

    return _getDataFromServer({facilityId, packageId, orderId}).then( response => { 
        let invoiceData = response.data.responseData;
        let barcodeText = invoiceData.packageCode;
        
        // log request params
        logger.log('debug', `Response from API`, invoiceData);
        logger.log('debug', `Now generating barcode for value=${barcodeText}`);

        let code128 = Barcode('code128', {
            data: barcodeText,
            width: 230,
            height: 50
        });

        code128.getBase64(function (err, barcodeImgSrc) {
            if (err) {
                let error = new Error();
                error.messageHeader = `Unable to print Invoice for Package ${packageId}`;
                error.message = `Unable to generate barcode of ${barcodeText}`;
                error.statusCode = 500;
                return next(error);
            }
            
            invoiceData.barcode = barcodeImgSrc;
            res.render('invoice', invoiceData);
        });
    }).catch((err) => {
        let error = new Error();
        error.messageHeader = `Unable to print Invoice for Package ${packageId}`;

        if(err.code === "ECONNREFUSED") {
            error.message = "API server is not responding!!"
        } else {
            error.message = err.response.data.message;
        }
        error.statusCode = 500;
        return next(error);
    });
    
}
